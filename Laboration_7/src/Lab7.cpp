//------------------------------------------------------------------------------
// Lab7.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab7.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab7.hpp"


int main()
{
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
