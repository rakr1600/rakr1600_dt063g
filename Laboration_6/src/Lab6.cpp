//------------------------------------------------------------------------------
// Lab6.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab6.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab6.hpp"


int main()
{
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
