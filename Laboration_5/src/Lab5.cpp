//------------------------------------------------------------------------------
// Lab5.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab5.h"
#include "../../_CodeBase/Hanoi/include/HanoiEngine.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab5.hpp"


int main()
{
    std::cout << getAssignmentInfo() << std::endl;
    // HanoiEngine engine = HanoiEngine(3);
    // engine.show();
    return 0;
}
