#include <vector>
#include <memory>


class BakingRecipe
{
private:
    std::string name;

protected:
    std::vector<Ingredient> neededIng;
    BakingRecipe(std::string aName) { this->name = aName; }

public:
    virtual ~BakingRecipe() = default;

    const std::string& getName() { return this->name; }

    bool isBakeable(std::vector<Ingredient> &availableIng)
    {
        for (int i = 0; i < this->neededIng.size(); i++)
        {
            bool result = false;
            for (int j = 0; j < availableIng.size(); j++)
            {
                if (this->neededIng[i] == availableIng[j])
                    result = true;
            }
            if (!result)
                return false;
        }
        return true;
    }

    virtual void bakeIt() = 0;
};

class Pizza : public BakingRecipe
{
private:

public:
    Pizza() : Pizza("Pizza") { }
    Pizza(std::string aName) : BakingRecipe(aName)
    {
        this->neededIng = {Ingredient("oil"), Ingredient("yeast"),
                           Ingredient("wheat-flour"), Ingredient("salt")};
    }

    void bakeIt()
    {
        std::cout << std::endl << "Bake " << this->getName() << std::endl;

        int i = 0;
        for (; i < this->neededIng.size(); i++)
            std::cout << i+1 << ": " << "Get " << this->neededIng[i].getName() << " out of pantry." << std::endl;
        
        std::cout << ++i << ": " << "Preheat oven to 450 degrees F (230 degrees C). In a medium bowl,"
        << std::endl << "dissolve yeast in warm water. Let stand until creamy, about 10 minutes." << std::endl;
        
        std::cout << ++i << ": " << "Stir in flour, salt and oil. Beat until smooth. Let rest for 5 minutes." << std::endl;
        std::cout << ++i << ": " << "Turn dough out onto a lightly floured surface and pat or roll into a round." << std::endl;
        std::cout << ++i << ": " << "Transfer crust to a lightly greased pizza pan or baker's peel dusted with cornmeal." << std::endl;
        
        std::cout << ++i << ": " << "Spread with desired toppings and bake in preheated oven for 15 to 20 minutes,"
        << std::endl << "or until golden brown. Let baked pizza cool for 5 minutes before serving." << std::endl;

        std::cout << this->getName() << " Completed" << std::endl;
    }
};

class Cake : public BakingRecipe
{
private:

public:
    Cake() : Cake("Cake") { }
    Cake(std::string aName) : BakingRecipe(aName)
    {
        this->neededIng = {Ingredient("egg"), Ingredient("sugar"),
                           Ingredient("baking-powder"), Ingredient("wheat-flour"),
                           Ingredient("salt"), Ingredient("marge"),
                           Ingredient("cocoa-powder")};
    }

    void bakeIt()
    {
        std::cout << std::endl << "Bake " << this->getName() << std::endl;

        int i = 0;
        for (; i < this->neededIng.size(); i++)
            std::cout << i+1 << ": " << "Get " << this->neededIng[i].getName() << " out of pantry." << std::endl;
        
        std::cout << ++i << ": " << "Preheat the oven to 180°C/fan160°C/gas 4. Grease 2 x 20cm sandwich tins "
        << std::endl << "with a little marge and base line with baking paper." << std::endl;

        std::cout << ++i << ": " << "In a bowl, beat together the marge and sugar with an electric hand whisk "
        << std::endl << "until light and fluffy." << std::endl;

        std::cout << ++i << ": " << "Add the eggs, 1 at a time, beating after each addition. Sift in the flour, "
        << std::endl <<"baking powder and cocoa, then lightly fold into the mixture." << std::endl;

        std::cout << ++i << ": " << "Add a little milk until you have a dropping consistency, then divide between "
        << std::endl << "the tins." << std::endl;

        std::cout << ++i << ": " << "Bake for 20-25 minutes or until a skewer inserted into the centre comes out"
        << std::endl << "clean." << std::endl;

        std::cout << ++i << ": " << "Leave to cool in the tin for 10 minutes, then turn out onto a wire rack to "
        << std::endl << "cool completely." << std::endl;

        std::cout << this->getName() << " Completed" << std::endl;
    }
};

class Scones : public BakingRecipe
{
private:

public:
    Scones() : Scones("Scones") { }
    Scones(std::string aName) : BakingRecipe(aName)
    {
        this->neededIng = {Ingredient("oil"), Ingredient("baking-powder"),
                           Ingredient("wheat-flour"), Ingredient("salt"),
                           Ingredient("sugar"), Ingredient("milk")};
    }

    void bakeIt()
    {
        std::cout << std::endl << "Bake " << this->getName() << std::endl;

        int i = 0;
        for (; i < this->neededIng.size(); i++)
            std::cout << i+1 << ": " << "Get " << this->neededIng[i].getName() << " out of pantry." << std::endl;
        
        std::cout << ++i << ": " << "Preheat oven to 400 degrees F (200 degrees C). Lightly grease a baking sheet." << std::endl;
        std::cout << ++i << ": " << "In a large bowl, combine flour, sugar, baking powder, and salt." << std::endl;
        std::cout << ++i << ": " << "Mix the milk in a small bowl, and stir into flour mixture until moistened" << std::endl;
        std::cout << ++i << ": " << "Turn dough out onto a lightly floured surface, and knead briefly." << std::endl;

        std::cout << ++i << ": " << "Roll dough out into a 1/2 inch thick round. Cut into 8 wedges, and place on"
        << std::endl << "the prepared baking sheet." << std::endl;

        std::cout << ++i << ": " << "Bake 15 minutes in the preheated oven, or until golden brown." << std::endl;

        std::cout << this->getName() << " Completed" << std::endl;
    }
};

struct NoBakingException : public std::exception
{
	const char * what () const throw ()
    {
    	return "This Recipe can't be prepared";
    }
};

class BakingrecipeManager
{
private:
    std::vector<std::unique_ptr<BakingRecipe>> bakingRecipes;
    std::vector<Ingredient> availableIngredients;

    int currentRecipe;

    bool hasAnotherRecipe()
    {
        return this->currentRecipe < this->bakingRecipes.size();
    }

public:
    BakingrecipeManager(std::string fileName)
    {
        std::cout << std::endl << "====================" << std::endl
        << "CONTENTS OF PANTRY"
        << std::endl << "====================" << std::endl;

        std::ifstream infile(fileName);
        if (!infile.is_open())
            std::cout << "unable to open file" << std::endl;

        std::string line;
        while (getline(infile, line))
        {
            std::cout << Ingredient(line).getName() << std::endl;

            if (!line.empty() && line[line.size() - 1] == '\r')
                line = line.erase(line.size() - 1);
            this->availableIngredients.push_back(Ingredient(line));
        }
        infile.close();
        std::cout << std::endl;

        this->bakingRecipes.push_back(std::move(std::unique_ptr<Pizza>{new Pizza()}));
        this->bakingRecipes.push_back(std::move(std::unique_ptr<Cake>{new Cake()}));
        this->bakingRecipes.push_back(std::move(std::unique_ptr<Scones>{new Scones()}));

        this->currentRecipe = 0;
    }

    std::unique_ptr<BakingRecipe> getNextBakingRecipe()
    {
        if (this->hasAnotherRecipe())
            return std::move(this->bakingRecipes[this->currentRecipe++]);
        else
            throw NoBakingException();
    }
};

class Bakery
{
private:
    std::unique_ptr<BakingrecipeManager> recipeManager;

public:
    Bakery()
    {
        this->recipeManager.reset(new BakingrecipeManager("_Resources/pantry.dat"));

        try
        {
            this->recipeManager->getNextBakingRecipe()->bakeIt();
            this->recipeManager->getNextBakingRecipe()->bakeIt();
            this->recipeManager->getNextBakingRecipe()->bakeIt();
            this->recipeManager->getNextBakingRecipe()->bakeIt();
        }
        catch (NoBakingException& e)
        {
            std::cout << e.what() << std::endl;
        }
    }
};
