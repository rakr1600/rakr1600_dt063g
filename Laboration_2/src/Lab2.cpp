//------------------------------------------------------------------------------
// Lab2.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab2.h"
#include <iostream>
#include <fstream>
#include "../../_CodeBase/Bakery/Ingredient.hpp"
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab2.hpp"


int main()
{
    std::cout << getAssignmentInfo() << std::endl;

    Bakery bakery;

    return 0;
}
