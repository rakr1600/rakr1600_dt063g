#include <memory>


/* Interface that produces sound */
class SoundProducer
{
private:
    std::string soundName;

public:
    SoundProducer(std::string nameOfSound)
    {
        soundName = nameOfSound;
    }
    virtual ~SoundProducer() = default;

    virtual void makeSound() = 0;

    std::string getSoundName()
    {
        return this->soundName;
    }
};

/* Class that whispers */
class Whisperer : public SoundProducer
{
private:

public:
    Whisperer() : Whisperer("Whisper") { }
    Whisperer(std::string nameOfSound) : SoundProducer(nameOfSound) { }

    void makeSound()
    {
        std::cout << this->getSoundName() << ": " << "Ssch,hush,hush" << std::endl;
    }
};

/* Class that shouts */
class Shouter : public SoundProducer
{
private:

public:
    Shouter() : Shouter("Shout") { }
    Shouter(std::string nameOfSound) : SoundProducer(nameOfSound) { }

    void makeSound()
    {
        std::cout << this->getSoundName() << ": " << "WOW YEEEH!!" << std::endl;
    }
};

/* Interface that talks */
class Talker
{
private:

public:
    virtual ~Talker() = default;
    
    virtual void saySomething() = 0;
};

/* Assignment Class handles SoundProducers */
class Uppgift1 : public Talker
{
private:
   std::unique_ptr<SoundProducer> sp;

public:
    Uppgift1() = default;

    void setSoundProducer(SoundProducer *sp)
    {
        this->sp.reset(sp);
    }

    void saySomething()
    {
        this->sp->makeSound();
    }
};
