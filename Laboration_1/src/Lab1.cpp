//------------------------------------------------------------------------------
// Lab1.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab1.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab1.hpp"


int main()
{
    Uppgift1 uppg1;

    uppg1.setSoundProducer(new Whisperer());
    uppg1.saySomething();

    uppg1.setSoundProducer(new Shouter());
    uppg1.saySomething();

    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
