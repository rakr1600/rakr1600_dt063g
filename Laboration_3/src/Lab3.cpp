//------------------------------------------------------------------------------
// Lab3.cpp DT063G Design Patterns With C++
//------------------------------------------------------------------------------

#include "../include/Lab3.h"
#include "../../_CodeBase/PowerSource/include/PowerSourceAdapters.h"
#include <iostream>
#include "../../_CodeBase/_libs/memstat.hpp"

#include "../include/Lab3.hpp"


int main()
{
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}
