//------------------------------------------------------------------------------
// GameFactory.cpp DT063G Design Patterns With C++
// Implementations of concrete factory classes
//------------------------------------------------------------------------------

#include "../include/GameFactory.h"
#include "../include/Obstacle.h"
#include "../include/Action.h"

/** Implementations of concrete factory classes */
